use fera;

drop table if exists users;

create table users (
	userid int primary key,
    name varchar(255) not null,
    email varchar(255) not null,
    password varchar(255) not null
);

create table reconstructions(
	recid int primary key,
    userid int not null,
    
    foreign key (userid) references users (userid)
);